import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public server: string = 'http://localhost:1902';
    private apiUrl: string = 'api';
    public baseurl: string = `${this.server}/${this.apiUrl}`;
}
