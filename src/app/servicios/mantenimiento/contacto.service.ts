import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../../../environments/configuration';
//import { Response } from '../../entidades/vo/response';
import { HttpClient/*, HttpErrorResponse*/ } from '@angular/common/http';
//import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ContactoService {

  private actionUrl: string;
  constructor(private http: HttpClient,
              private configuration: Configuration) { 
    this.actionUrl = configuration.baseurl;
  }

  getAll = (url: string, filter: any): Observable<any> => {
    return this.http.post<any>(`${this.actionUrl}/${url}`, filter).catch(this.handleError);
  }


  get = (url: string): Observable<any> => {
      return this.http.get<any>(`${this.actionUrl}/${url}`)
          .catch(this.handleError);
  }

  post = (url: string, entity: any): Observable<any> => {
      return this.http.post<any>(`${this.actionUrl}/${url}`, entity).catch(this.handleError);
  }

  put = (url: string, entity: any): Observable<any> => {
      return this.http.put<any>(`${this.actionUrl}/${url}`, entity).catch(this.handleError);
  }

  delete = (url: string): Observable<any> => {
      return this.http.delete<any>(`${this.actionUrl}/${url}`).catch(this.handleError);
  }

  private handleError(error: HttpResponse<any>) {
      console.error(error);
      return Observable.throw(error || 'Server error');
  }

}
