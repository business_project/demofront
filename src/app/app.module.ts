import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
//import { HttpClient } from '@angular/common/http';
import { NgxPaginationModule } from '../../Resources/ngx-pagination';

//import { CoreModule } from '../core/core.module';
import { MantenimientoModule } from './componentes/areas/mantenimiento/mantenimiento.module';
import { AppRoutes } from './app.routes';

import { AppComponent } from './app.component';
import { AsideComponent } from './componentes/areas/shared/master/aside/aside.component';
import { ContentComponent } from './componentes/areas/shared/master/content/content.component';
import { FooterComponent } from './componentes/areas/shared/master/footer/footer.component';
import { HeaderComponent } from './componentes/areas/shared/master/header/header.component';
import { ErrorViewerComponent } from './componentes/areas/shared/helpers/error-viewer/error-viewer.component';

import { ContactoService } from './servicios/mantenimiento/contacto.service'

import { HttpClientModule } from '@angular/common/http';
import { Configuration } from '../environments/configuration';


@NgModule({
  declarations: [
    AppComponent,
    AsideComponent,
    ContentComponent,
    FooterComponent,
    HeaderComponent,
    ErrorViewerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes, { useHash: false, preloadingStrategy: PreloadAllModules }),
    //CoreModule.forRoot(),
    MantenimientoModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [
    ContactoService,
    Configuration
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
