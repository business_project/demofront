import { Routes } from '@angular/router';

import { ContactoListComponent } from './contacto/contacto-list/contacto-list.component';
import { ContactoEditorComponent } from './contacto/contacto-editor/contacto-editor.component';
import { ContactoViewerComponent } from './contacto/contacto-viewer/contacto-viewer.component';

export const MantenimientoRoutes: Routes = [
    {path: 'mantenimiento/contactos', component: ContactoListComponent},
    {path: 'mantenimiento/contacto/crear', component: ContactoEditorComponent},
    {path: 'mantenimiento/contacto/editar/:id', component: ContactoEditorComponent},
    {path: 'mantenimiento/contacto/ver/:id', component: ContactoViewerComponent}
];