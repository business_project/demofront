import { Component, OnInit/*, OnDestroy*/ } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Contacto } from '../../../../../entidades/entity/contacto'
import { ContactoService } from '../../../../../servicios/mantenimiento/contacto.service'

@Component({
  selector: 'app-contacto-editor',
  templateUrl: './contacto-editor.component.html',
  styles: []
})
export class ContactoEditorComponent implements OnInit {

  private form: FormGroup;
  private subscribeParams: any;
  private subscribeGet: any;
	private subscribeSave: any;
  private id: number = 0;
  private entidad: Contacto = new Contacto();

  constructor(
    //private readonly _fb: FormBuilder,
    private readonly _router: Router,
		private readonly _activeRouter: ActivatedRoute,
		private readonly _contactoService: ContactoService
  ) { 
    this.createForm();
  }

  ngOnInit() {
    this.getParams();
  }

  getParams() {
		this.subscribeParams = this._activeRouter.params.subscribe(params => {
			this.id = +params['id'];
			if (this.id && this.id > 0) {
				this.getById();
			}
		});
	}

  getById() {
		if (this.id && this.id > 0) {

			//let me = this;
			this.subscribeGet = this._contactoService.get(`contacto/${this.id}`).subscribe(response => {
				console.info(response);
				if (response) {

						this.entidad = response as Contacto;
						this.form.setValue({
							Nombre: this.entidad.Nombre,
							Correo: this.entidad.Email,
							Telefono: this.entidad.Telefono
						})

						// me.entidad = response as Contacto;
						// me.form.setValue({
						// 		Id: me.id,
						// 		Nombre: me.entidad.Nombre,
						// 		Correo: me.entidad.Email,
						// 		Telefono: me.entidad.Telefono
						// });

            //this.markFormGroupTouched(me.form);

				} else {
					console.error('ocurrió un error');
				}
			}, errors => {
				console.error("Errooooooorrrr: ", errors);
			});
		}
	}

  // markFormGroupTouched(formGroup: FormGroup) {
  //   (<any>Object).values(formGroup.controls).forEach(control => {
  //     control.markAsTouched();

  //     if (control.controls) {
  //       control.controls.forEach(c => this.markFormGroupTouched(c));
  //     }
  //   });
  // }

	createForm() {

		this.form = new FormGroup({
			Nombre: new FormControl('', [Validators.required, Validators.maxLength(150)]),
			Correo: new FormControl('', [Validators.required, Validators.maxLength(150)]),
			Telefono: new FormControl('', [Validators.required, Validators.maxLength(20)]),
		})
    // this.form = this._fb.group({
		// 	Id: 0,
		// 	Nombre: 			['', Validators.compose([Validators.required,
		// 												Validators.maxLength(150)])],
		// 	Correo: 			['', Validators.compose([Validators.required,
		// 												Validators.maxLength(150)])],
		// 	Telefono: 			['', Validators.compose([Validators.required,
		// 												Validators.maxLength(20)])]
    // 	});
	};
	
	update(){
		if(this.form.value){
			this.entidad.IdContacto = this.id;
			this.entidad.Nombre = this.form.controls['Nombre'].value;
			this.entidad.Email = this.form.controls['Correo'].value;
			this.entidad.Telefono = this.form.controls['Telefono'].value;

			let method = this.id && this.id > 0 ? this._contactoService.put(`contacto/${this.id}`, this.entidad) : this._contactoService.post('contacto', this.entidad);
			this.subscribeSave = method.subscribe(response => {
				if (response) {
						this._router.navigate(['/mantenimiento/contactos']);
				} else {
					console.error('ocurrió un error');
				}
			}, errors => {
				console.error("Errooooooorrrr: ", errors);
			});
		}
	}


}
