import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../../../../servicios/mantenimiento/contacto.service'
import { Contacto } from '../../../../../entidades/entity/contacto'
import { FiltroBaseDto } from '../../../../../entidades/dto/filtro-base.dto'
import {  } from '../../../../../../Resources/ngx-pagination'
//import { Response } from '../../../../entidades/vo/response';

@Component({
  selector: 'app-contacto-list',
  templateUrl: './contacto-list.component.html',
  styles: []
})
export class ContactoListComponent implements OnInit {

  private lista: Contacto[] = [];
  private filter: FiltroBaseDto = new FiltroBaseDto();

  constructor(private contactoService:ContactoService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.contactoService.getAll('contacto/Get', this.filter)
    .subscribe(data =>{
      this.lista = data;
      console.log(this.lista);
    })
  }

  filterChange(newValue) {
    this.getAll();
  };

  remove(id:number){
    console.log(id);


    this.getAll();
  }

}
