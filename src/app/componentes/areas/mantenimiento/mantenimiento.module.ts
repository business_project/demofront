import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { ErrorViewerComponent} from '../shared/helpers/error-viewer/error-viewer.component';
import { MantenimientoRoutes } from './mantenimiento.routes';
//import { NgxPaginationModule } from '../../helpers/ngx-pagination/dist/ngx-pagination';
//import { ReactiveFormsHelper} from '../../export/helper.export';

//Componentes
import { ContactoListComponent } from './contacto/contacto-list/contacto-list.component';
import { ContactoEditorComponent } from './contacto/contacto-editor/contacto-editor.component';
import { ContactoViewerComponent } from './contacto/contacto-viewer/contacto-viewer.component';

@NgModule({
    imports: [
			CommonModule,
			RouterModule.forChild(MantenimientoRoutes),
			//NgxPaginationModule,
			FormsModule,
			ReactiveFormsModule
    ],
    declarations: [
			//ErrorViewerComponent,
			ContactoListComponent,
			ContactoEditorComponent,
			ContactoViewerComponent
    ],
    exports: []
})
export class MantenimientoModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: MantenimientoModule,
            providers: [
							//ReactiveFormsHelper
						]
        }
    }
}
