export class Contacto {
    
        public IdContacto: number | null | undefined;
        public Nombre: string | null | undefined;
        public Email: string | null | undefined;
        public Telefono: string | null | undefined;
    
    }